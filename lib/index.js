// API-KIT
// JSON Web Token Compatibility Update
// Refactored Nov 2018

// LocalStorage Polyfill
if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./ls');
}

// Modules
const uuid = require('uuid');
const axios = require('axios');

// Class Construction
class _API {
  constructor(options, isNode) {
    // Default API options
    let defaultOptions = {
      server: 'api.mgallery.center',
      protocol: 'https://',
      deviceId: '',
      jwt: null,
      isNode
    };
    if (options) {
      if (options.hasOwnProperty('server')) defaultOptions.server = options.server;
      if (options.hasOwnProperty('protocol')) defaultOptions.protocol = options.protocol;
    }
    let currentUser = null;
    this.currentUser = currentUser;
    // Load device ID
    if (isNode) {
      defaultOptions.deviceId = 'DID' + uuid.v4().toUpperCase();
    } else {
      if (localStorage.getItem('__APIKIT_DEVICEID') == null) {
        // Generate new one
        const newDid = 'DID' + uuid.v4().toUpperCase();
        localStorage.setItem('__APIKIT_DEVICEID', newDid);
        defaultOptions.deviceId = newDid;
      } else {
        const thisDid = localStorage.getItem('__APIKIT_DEVICEID');
        defaultOptions.deviceId = thisDid;
      }
      // JWT
      if (localStorage.getItem('__TOKEN') != null) {
        // Fetch session ID from localStorage
        const jwt = localStorage.getItem('__TOKEN');
        defaultOptions.jwt = jwt;
      }
    }
    this._OPTIONS = defaultOptions;
  };
  async _getCurrentUser() {
    let vm = this;
    if (this.currentUser) {
      return new Promise(res => {
        res(vm.currentUser);
      });
    }
    return this._request('/v2/users/currentUser').then(function (u) {
      vm.currentUser = u;
      return u;
    }).catch(e => null);
  };
  /**
   * Raw request method
   * @param {String} endpoint 
   * @param {Object} [body]
   * @param {Object} [query]
   * @param {Object} [headerOverride]
   * @returns {Promise} promise
   */
  async _request(endpoint, body, query, headerOverride) {
    const vm = this;
    const _uri = vm._OPTIONS.protocol + vm._OPTIONS.server + endpoint;

    let headers = {
      'api-kit-device-id': vm._OPTIONS.deviceId,
      'Accept': 'application/json,text/plain, */*',
      'Authorization': vm._OPTIONS.jwt ? ('Bearer ' + vm._OPTIONS.jwt) : null
    };

    headers = Object.assign(headers, headerOverride);

    return await axios({
      method: 'post',
      url: _uri,
      data: body,
      headers
    }).then(result => {
      return result.data;
    });
    
  };
  /**
   * Send login request for JWT
   * @param {Object} credentials
   */
  async login(cred) {
    let vm = this;
    return await vm._request('/v2/users/login', cred).then(res => {
      localStorage.setItem('__TOKEN', res.token);
      vm._OPTIONS.jwt = res.token;
      return res;
    }).then(async () => {
      return await vm._getCurrentUser.call(vm);
    });
  };
  /**
   * Send logout request and clears local info
   */
  async logout() {
    let vm = this;
    if (vm.currentUser) {
      return vm._request('/v2/users/logout').then(r => {
        if (r.success) {
          localStorage.setItem('__TOKEN', null);
          vm._OPTIONS.jwt = null;
          vm.currentUser = null;
        }
      });
    }
  };
  /**
   * Alias: request
   */
  async request(endpoint, body, query, headerOverride) {
    return await this._request(endpoint, body, query, headerOverride);
  }
};
_API.prototype.Request = _API.prototype._request;

module.exports = function (options, isNode) {
  return new _API(options, isNode);
};
