# api-kit
對應 API Server 的前端包裝函式庫

## 快速開始

作為 npm 模組安裝：

```bash
$ npm i https://bitbucket.org/9dynamics/api-kit.git --save
```

然後在 `.js` 或是 `.vue` 檔案中引入：

```javascript
// apiOptions 只有在測試環境才需要傳入
// 否則預設是 https://api.mgallery.center/
const apiOptions = {
  protocol: 'https://',
  server: 'api-test.mgallery.center'
};
const api = require('api-kit')(apiOptions);
```
